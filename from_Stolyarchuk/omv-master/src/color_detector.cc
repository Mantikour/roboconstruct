#include "omv/color_detector.h"

#include "omv/logger.h"

namespace omega {

ColorDetector::ColorDetector() {}

Frame ColorDetector::Threshold(Frame& frame, const std::array<int, 3>& blur_kernels) {
  Frame frame_thresholded;
  /*
  Frame frame_gray;

  cv::Size g_blur_kernel{blur_kernels[0], blur_kernels[1]};
  int m_blur_kernel = blur_kernels[2];

  cv::medianBlur(frame.m, frame.m, m_blur_kernel);
  cv::cvtColor(frame.m, frame_gray.m, cv::COLOR_BGR2GRAY);
  cv::Canny(frame_gray.m, frame_thresholded.m, 400, 1000, 3);
  cv::GaussianBlur(frame_thresholded.m, frame_thresholded.m, g_blur_kernel, 0);
  //  cv::threshold(frame_thresholded.m, frame_thresholded.m, 60, 255, cv::THRESH_BINARY);

  ErodeFrame(frame_thresholded);

  //  cv::bitwise_not(frame_thresholded.m, frame_thresholded.m);

  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(frame_thresholded.m, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

  std::vector<cv::Point> approx;
  for (const auto& contour : contours) {
    cv::approxPolyDP(contour, approx, cv::arcLength(contour, true) * 0.02, true);

    if (cv::contourArea(contour) > 1000 && approx.size() > 4) {
      LOG(DEBUG) << approx.size();
      cv::Rect r4 = cv::boundingRect(contour);

      cv::rectangle(frame.m, r4, cv::Scalar{255, 0, 0}, 2, 4);
    }
  }

  //  ShowFrame(frame_gray, "gray");
  //  ShowFrame(frame_thresholded, "thrs");
  */
  return frame_thresholded;
}

Frame ColorDetector::Threshold(Frame& frame, const Color& color, const std::array<int, 3>& blur_kernels) {
  Frame frame_hsv;
  Frame frame_thresholded;

  cv::Size g_blur_kernel{blur_kernels[0], blur_kernels[1]};
  int m_blur_kernel = blur_kernels[2];

  cv::medianBlur(frame.m, frame.m, m_blur_kernel);
  cv::cvtColor(frame.m, frame_hsv.m, cv::COLOR_BGR2HSV);
  cv::Vec3b hsv_color = color.ToHSV();

  LOG(DEBUG) << color.BGR() << ' ' << hsv_color << ' ' << cv::Vec3b(0, hsv_color[1] + 40, hsv_color[2] + 40);

  auto calc_sv_borders = [](uint8_t val) {
    if (val < 64)
      return std::pair<uchar, uchar>{0, 96};
    else if (val >= 64 && val < 96)
      return std::pair<uchar, uchar>{32, 128};
    else if (val >= 96 && val < 128)
      return std::pair<uchar, uchar>{64, 160};
    else if (val >= 128 && val < 160)
      return std::pair<uchar, uchar>{96, 192};
    else if (val >= 160 && val < 192)
      return std::pair<uchar, uchar>{128, 224};
    else if (val >= 192 && val < 224)
      return std::pair<uchar, uchar>{160, 255};
    else
      return std::pair<uchar, uchar>{192, 255};
  };

  auto [s_min, s_max] = calc_sv_borders(hsv_color[1]);
  auto [v_min, v_max] = calc_sv_borders(hsv_color[2]);

  cv::Mat mask, mask1;
  uchar h = hsv_color[0];

  if (h < 10) {
    uchar h_min = 0, h_max = h + 10, h_min1 = 170, h_max1 = 179;
    cv::inRange(frame_hsv.m, cv::Vec3b{h_min, s_min, v_min}, cv::Vec3b{h_max, s_max, v_max}, mask);
    cv::inRange(frame_hsv.m, cv::Vec3b{h_min1, s_min, v_min}, cv::Vec3b{h_max1, s_max, v_max}, mask1);
    frame_thresholded.m = mask + mask1;
  } else if (h > 170) {
    uchar h_min = 0, h_max = 10, h_min1 = h - 10, h_max1 = 179;
    cv::inRange(frame_hsv.m, cv::Vec3b{h_min, s_min, v_min}, cv::Vec3b{h_max, s_max, v_max}, mask);
    cv::inRange(frame_hsv.m, cv::Vec3b{h_min1, s_min, v_min}, cv::Vec3b{h_max1, s_max, v_max}, mask1);
    frame_thresholded.m = mask + mask1;
  } else {
    uchar h_min = h - 10, h_max = h + 10;
    cv::inRange(frame_hsv.m, cv::Vec3b{h_min, s_min, v_min}, cv::Vec3b{h_max, s_max, v_max}, frame_thresholded.m);
  }
  cv::GaussianBlur(frame_thresholded.m, frame_thresholded.m, g_blur_kernel, 2, 2);

  return frame_thresholded;
}

Frame ColorDetector::Threshold(Frame& frame, const cv::Vec3b& lower, const cv::Vec3b& upper,
                               const std::array<int, 3>& blur_kernels) {
  Frame frame_hsv;
  Frame frame_tmp;
  Frame frame_thresholded;

  cv::Size g_blur_kernel{blur_kernels[0], blur_kernels[1]};
  int m_blur_kernel = blur_kernels[2];

  //  cv::medianBlur(frame.m, frame_tmp.m, m_blur_kernel);
  cv::GaussianBlur(frame.m, frame_tmp.m, g_blur_kernel, 2);
  //  cv::GaussianBlur(frame_thresholded.m, frame_thresholded.m, g_blur_kernel, 0);
  cv::cvtColor(frame_tmp.m, frame_hsv.m, cv::COLOR_BGR2HSV);
  cv::inRange(frame_hsv.m, lower, upper, frame_thresholded.m);

  return frame_thresholded;
}

void ColorDetector::Erode(Frame& frame) {
  cv::erode(frame.m, frame.m, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
  cv::dilate(frame.m, frame.m, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));

  cv::dilate(frame.m, frame.m, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
  cv::erode(frame.m, frame.m, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
}

}  // namespace omega
