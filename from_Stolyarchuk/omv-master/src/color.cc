#include "omv/color.h"

namespace omega {

std::map<std::string, cv::Range> Color::basic_colors{{"orange", cv::Range{0, 22}},    {"yellow", cv::Range{22, 38}},
                                                     {"green", cv::Range{38, 75}},    {"blue", cv::Range{75, 130}},
                                                     {"violet", cv::Range{130, 160}}, {"red", cv::Range{160, 179}}};

cv::Vec3b Color::ToHSV() const {
  cv::Mat3b m_bgr(bgr_);
  cv::Mat3b m_hsv;

  cv::cvtColor(m_bgr, m_hsv, cv::COLOR_BGR2HSV);

  return m_hsv.at<cv::Vec3b>(0, 0);
}

cv::Vec3b Color::BGR() const {
  return bgr_;
}

}  // namespace omega
