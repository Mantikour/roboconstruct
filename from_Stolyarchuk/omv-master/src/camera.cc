#include "omv/camera.h"

#include "omv/logger.h"

namespace omega {

Camera::Camera(int camera_no) : capture_device_(camera_no), capturing_(true) {}

Camera::~Camera() {
  if (capture_thread_->joinable())
    capture_thread_->join();
}

void Camera::RegisterCallback(const CameraCallback& callback) {
  callback_ = callback;
}

void Camera::Start(int frame_rate) {
  //  Frame frame;
  //  capture_device_ >> frame;

  //  stream_.write(frame.m);

  //  cv::resize(frame.m, frame.m, cv::Size(640, 360));

  //  frame.m.release();

  //  stream_.start();

  if (frame_rate > 0)
    capture_device_.set(cv::CAP_PROP_FPS, frame_rate);

  capture_thread_.reset(new std::thread([&]() {
    while (capturing_) {
      Frame frame;
      capture_device_ >> frame;

      //      cv::resize(frame1.m, frame1.m, cv::Size(640, 360));

      //      stream_.write(frame1.m);
      //      frame1.m.release();

      if (!frame.m.empty())
        callback_(std::move(frame));
    }
  }));
}

void Camera::Stop() {
  LOG(INFO) << "stopping camera";
  capturing_ = false;
}

}  // namespace omega
