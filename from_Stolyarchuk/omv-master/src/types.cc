#include "omv/types.h"

#include "omv/logger.h"

namespace omega {

cv::VideoCapture& operator>>(cv::VideoCapture& cap, Frame& frame) {
  return cap >> frame.m;
}

Frame::Frame(const std::string& file) {
  m = cv::imread(file);
}

Rect::Rect(const cv::Rect& rect) {
  FromCvRect(rect);
}

Rect::Rect(const cv::Rect2f& rect) {
  FromCvRect(rect);
}

Rect::Rect(const std::vector<cv::Point2f>& contour) {
  FromCvContour(contour);
}

Rect::Rect(const std::vector<cv::Point>& contour) {
  FromCvContour(contour);
}

bool Rect::PolygonTest(const std::vector<cv::Point>& search_contour) {
  bool result = false;

  for (auto& vert : verts)
    if (cv::pointPolygonTest(search_contour, cv::Point2f(vert.x, vert.y), false) > 0) {
      result = true;
      break;
    }

  return result;
}

cv::Rect Rect::ToRect() const {
  int x = static_cast<int>(verts[0].x);
  int y = static_cast<int>(verts[0].y);

  int width = static_cast<int>(verts[1].x - verts[0].x);
  int height = static_cast<int>(verts[3].y - verts[0].y);

  return cv::Rect(x, y, width, height);
}

void Rect::FromCvRect(const cv::Rect& rect) {
  verts[0].x = rect.x;
  verts[0].y = rect.y;

  verts[1].x = rect.x + rect.width;
  verts[1].y = rect.y;

  verts[2].x = rect.x + rect.width;
  verts[2].y = rect.y + rect.height;

  verts[3].x = rect.x;
  verts[3].y = rect.y + rect.height;
}

void Rect::FromCvContour(const std::vector<cv::Point>& contour) {
  if (contour.size() != 4)
    throw std::invalid_argument(cv::format("wrong nunmber of verticies: %lu. required: 4", contour.size()));

  for (size_t i = 0; i < contour.size(); ++i)
    verts[i] = Coords{contour[i]};
}

void Rect::FromCvContour(const std::vector<cv::Point2f>& contour) {
  if (contour.size() != 4)
    throw std::invalid_argument(cv::format("wrong nunmber of verticies: %lu. required: 4", contour.size()));

  for (size_t i = 0; i < contour.size(); ++i)
    verts[i] = Coords{contour[i]};
}

std::ostream& operator<<(std::ostream& os, const Coords& coords) {
  return os << "[x: " << coords.x << "; y: " << coords.y << ']';
}

std::ostream& operator<<(std::ostream& os, const Rect& rect) {
  return os << rect.verts[0] << ' ' << rect.verts[1] << ' ' << rect.verts[2] << ' ' << rect.verts[3];
}

Coords::Coords(float x_, float y_) {
  x = x_;
  y = y_;
}

Coords::Coords(const cv::Point& point) {
  x = point.x;
  y = point.y;
}

Coords::Coords(const cv::Point2f& point) {
  x = point.x;
  y = point.y;
}

Shape::~Shape() {}

}  // namespace omega
