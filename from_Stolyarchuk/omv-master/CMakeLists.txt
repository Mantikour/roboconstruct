cmake_minimum_required(VERSION 3.12)

project(omv)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-error")

find_package(OpenCV 4.1.1 EXACT COMPONENTS core dnn highgui imgcodecs videoio imgproc)

set(SRC
  ${CMAKE_CURRENT_SOURCE_DIR}/src/omv.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/camera.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/logger.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/color.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/color_detector.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/shape_detector.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/streamer.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/types.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/src/utils.cc)


add_library(${PROJECT_NAME} SHARED ${SRC})
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include ${OpenCV_INCLUDE_DIRS})

set(CMAKE_INSTALL_PREFIX /usr/local)
install (TARGETS ${PROJECT_NAME} LIBRARY DESTINATION lib64)

set(ignoreQt ${QT_QMAKE_EXECUTABLE})

