#ifndef UTILS_H
#define UTILS_H

#include <algorithm>

namespace omega {

namespace helpers {
template <typename Cont, typename It>
auto FilterIndices(Cont& cont, It beg, It end) -> decltype(std::end(cont)) {
  int helpIndx(0);
  return std::stable_partition(std::begin(cont), std::end(cont),                //
                               [&](decltype(*std::begin(cont)) const&) -> bool  //
                               { return std::find(beg, end, helpIndx++) == end; });
}

}  // namespace helpers

}  // namespace omega

#endif  // UTILS_H
