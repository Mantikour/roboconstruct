#ifndef TYPES_H
#define TYPES_H

#include <memory>
#include <vector>

#include <opencv2/opencv.hpp>

namespace omega {

struct Rect;

struct Coords {
  Coords() = default;
  Coords(float, float);
  Coords(const cv::Point&);
  Coords(const cv::Point2f&);

  float x;
  float y;
};

std::ostream& operator<<(std::ostream& os, const Coords& coords);

struct Frame {
  Frame() = default;
  Frame(const std::string&);
  Frame(int w, int h) {
    m = cv::Mat::zeros(cv::Size(w, h), CV_8UC3);
  }

  void Resize(int w, int h) {
    cv::resize(m, m, cv::Size(w, h));
  }
  cv::Mat m;
};

cv::VideoCapture& operator>>(cv::VideoCapture& cap, Frame& frame);

struct Shape : public std::enable_shared_from_this<Shape> {
  using Ptr = std::shared_ptr<Shape>;
  virtual ~Shape();

  template <typename T>
  std::shared_ptr<T> As() {
    return std::static_pointer_cast<T>(shared_from_this());
  }

  virtual bool PolygonTest(const std::vector<cv::Point>&) = 0;
  virtual std::string Debug() const {
    return std::string{};
  }

  inline friend std::ostream& operator<<(std::ostream& os, const Shape& shape) {
    return os << shape.Debug();
  }
};

struct Rect : public Shape {
  Rect(const cv::Rect&);
  Rect(const cv::Rect2f&);
  Rect(const std::vector<cv::Point2f>&);
  Rect(const std::vector<cv::Point>&);
  ~Rect() override = default;

  std::array<Coords, 4> verts;

  bool PolygonTest(const std::vector<cv::Point>& search_contour) override;
  cv::Rect ToRect() const;

  std::string Debug() const override {
    return std::string{};
  }

 private:
  void FromCvRect(const cv::Rect&);
  void FromCvContour(const std::vector<cv::Point>&);
  void FromCvContour(const std::vector<cv::Point2f>&);
};

std::ostream& operator<<(std::ostream& os, const Rect& rect);

struct Circle : public Shape {
  Coords center;
  size_t radius;

  virtual bool PolygonTest(const std::vector<cv::Point>&) {
    return true;
  }
};

struct Line : public Shape {
  std::array<Coords, 2> verts;

  virtual bool PolygonTest(const std::vector<cv::Point>&) {
    return true;
  }
};

struct Triangle : public Shape {
  std::array<Coords, 3> verts;

  virtual bool PolygonTest(const std::vector<cv::Point>&) {
    return true;
  }
};

using Result = std::vector<Shape::Ptr>;
using Contour = std::vector<cv::Point>;
using Contours = std::vector<Contour>;
using MaxSize = std::numeric_limits<size_t>;

}  // namespace omega

#endif  // TYPES_H
