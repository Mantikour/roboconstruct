#ifndef HANDLER_H
#define HANDLER_H

#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <unistd.h>

#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include "utility"

#include "common/logger.h"

class Socket;

struct TimerInvoker {
  template <typename F>
  void Run(F&& f, epoll_event&& event) {
    uint64_t value;
    read(event.data.fd, &value, 8);
    f(value);
  }
};

struct ConsoleInvoker {
  template <typename F>
  void Run(F&& f, epoll_event&& event) {
    std::string info;

    if (event.events & EPOLLIN) {
      getline(std::cin, info);
      f(info);
    }
  }
};

struct SignalsInvoker {
  template <typename F>
  void Run(F&& f, epoll_event&& event) {
    int fd = event.data.fd;
    signalfd_siginfo info;
    read(fd, &info, sizeof(signalfd_siginfo));
    //    if (read(fd, &info, sizeof(signalfd_siginfo)) !=
    //    sizeof(signalfd_siginfo))
    //      throw std::runtime_error("SignalsHandler");

    f(info.ssi_signo);
  }
};

struct AcceptInvoker {
  template <typename F>
  void Run(F&& f, epoll_event&& event) {
    int fd = event.data.fd;
    int session_fd = -1;
    sockaddr_in remote_addr;
    socklen_t session = sizeof(remote_addr);
    {
      std::lock_guard<std::mutex> lock(mutex);
      session_fd = accept(fd, (sockaddr*)&remote_addr, &session);
    }
    f(session_fd);
  }
  std::mutex mutex;
};

struct BaseReadSomeInvoker {
  using TcpCallbackSig = std::function<void(std::vector<char>&, ssize_t)>;
  using UdpCallbackSig = std::function<void(std::vector<char>&, ssize_t,
                                            const sockaddr*, socklen_t)>;

  template <class First, class... Args>
  BaseReadSomeInvoker(First first, Args&&... args) : kMaxBytes(first) {
    ;
  }

  template <typename F>
  void Run(F&& f, epoll_event&& event) {
    RunImpl(std::move(f), std::move(event));
  }

  virtual void RunImpl(TcpCallbackSig&& f, epoll_event&& event) = 0;
  virtual void RunImpl(UdpCallbackSig&& f, epoll_event&& event) = 0;

  const uint16_t kMaxBytes;
  std::mutex op_mutex;
};

struct ReadSomeInvoker : public BaseReadSomeInvoker {
  using BaseReadSomeInvoker::BaseReadSomeInvoker;

  void RunImpl(TcpCallbackSig&& f, epoll_event&& event) override {
    int fd = event.data.fd;
    std::vector<char> data(kMaxBytes);
    ssize_t len = -1;
    {
      std::lock_guard<std::mutex> lock(op_mutex);
      len = recv(fd, &data[0], kMaxBytes, 0);
    }
    f(data, len);
  }
  void RunImpl(UdpCallbackSig&& f, epoll_event&& event) override {
    int fd = event.data.fd;
    std::vector<char> data(kMaxBytes);
    sockaddr_in cliaddr;
    socklen_t socklen = sizeof(cliaddr);
    ssize_t len = -1;
    {
      std::lock_guard<std::mutex> lock(op_mutex);
      len = recvfrom(fd, &data[0], kMaxBytes, 0, (sockaddr*)&cliaddr, &socklen);
    }
    f(data, len, (const sockaddr*)&cliaddr, socklen);
  }
};

struct Handler {
  using Ptr = std::shared_ptr<Handler>;
  using Func = std::function<void()>;

  virtual ~Handler() {}

  virtual void Exec(epoll_event&& event) = 0;
  virtual void LogError() = 0;

  epoll_event e;
};

template <typename T, typename I>
struct HandlerT : public Handler {
  using Ptr = std::shared_ptr<HandlerT>;
  using Sig = std::function<T>;

  template <typename... Args>
  HandlerT(Sig&& func, Args&&... args) : func_(std::move(func)) {
    invoker_ = std::make_shared<I>(args...);
  }

  void Exec(epoll_event&& event) override {
    if (event.events | EPOLLERR)
      invoker_->Run(std::move(func_), std::move(event));
    else
      LOG(ERROR) << "epoll handle failed";
  }

  virtual void LogError() {}

 private:
  std::function<T> func_;
  std::shared_ptr<I> invoker_;
};

using TimerHandler = HandlerT<void(uint64_t), TimerInvoker>;
using ConsoleHandler = HandlerT<void(const std::string&), ConsoleInvoker>;
using SignalsHandler = HandlerT<void(uint32_t), SignalsInvoker>;
using AcceptHandler = HandlerT<void(int), AcceptInvoker>;
using TcpReadSomeHandler =
    HandlerT<void(std::vector<char>&, ssize_t), ReadSomeInvoker>;
using UdpReadSomeHandler =
    HandlerT<void(std::vector<char>&, ssize_t, const sockaddr*, socklen_t),
             ReadSomeInvoker>;

#endif  // HANDLER_H
