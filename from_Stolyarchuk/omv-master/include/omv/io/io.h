#ifndef IO_H
#define IO_H

#include <atomic>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <vector>

#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <unistd.h>

#include "handler.h"
#include "queue.h"

#define EPOLL_MAX_EVENTS 64

class Io {
 public:
  Io(Queue &);
  ~Io();

  void Run();
  void Stop();

  void Register(Handler::Ptr handler);
  void Handle(epoll_event &);
  void Release(int);

 private:
  Queue &queue_;

  int epoll_fd_;
  int close_fd_;

  bool stopped_;

  std::map<int, Handler::Ptr> handlers_;
  //  std::mutex handlers_mutex_;
};

#endif  // IO_H
