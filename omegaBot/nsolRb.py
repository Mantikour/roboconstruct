
import cv2
import numpy as np
import serial as sp
import math as mt

if __name__ == '__main__':
    def nothing(*arg):
        pass

cv2.namedWindow( "result" ) # создаем главное окно
cv2.namedWindow( "settings" ) # создаем окно настроек
cap = cv2.VideoCapture(0) # видеозахват

# создаем 6 бегунков для настройки начального и конечного цвета фильтра
cv2.createTrackbar('h1', 'settings', 0, 255, nothing)
cv2.createTrackbar('s1', 'settings', 0, 255, nothing)
cv2.createTrackbar('v1', 'settings', 0, 255, nothing)
cv2.createTrackbar('h2', 'settings', 255, 255, nothing)
cv2.createTrackbar('s2', 'settings', 255, 255, nothing)
cv2.createTrackbar('v2', 'settings', 255, 255, nothing)


while True:
    flag, img = cap.read()
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV )
 
    # считываем значения бегунков
    h1 = cv2.getTrackbarPos('h1', 'settings')
    s1 = cv2.getTrackbarPos('s1', 'settings')
    v1 = cv2.getTrackbarPos('v1', 'settings')
    h2 = cv2.getTrackbarPos('h2', 'settings')
    s2 = cv2.getTrackbarPos('s2', 'settings')
    v2 = cv2.getTrackbarPos('v2', 'settings')
    
    
    h_min = np.array((h1, s1, v1), np.uint8)
    h_max = np.array((h2, s2, v2), np.uint8)

    # накладываем фильтр на кадр в модели HSV
    thresh = cv2.inRange(hsv, h_min, h_max)
    
          # вычисляем моменты изображения
    moments = cv2.moments(thresh, 1)
    dM01 = moments['m01']
    dM10 = moments['m10']
    dArea = moments['m00']
    
    if dArea > 100:
        x = int(dM10 / dArea)
        y = int(dM01 / dArea)
        contours, hierarchy = cv2.findContours( thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #cv2.drawContours( thresh, contours, -1, (180,180,180), 3, cv2.LINE_AA, hierarchy, 1 )
        cv2.circle(thresh, (x, y), 5, (255,255,255), -1)
        cv2.putText(thresh, "%d-%d" % (x,y), (x+20,y-20), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)

    
        # перебираем все найденные контуры в цикле
    for cnt in contours:
        rect = cv2.minAreaRect(cnt) #  прямоугольник
        box = cv2.boxPoints(rect) # поиск вершин прямоугольника
        box = np.int0(box) # округление координат
        area = int(rect[1][0]*rect[1][1]) # площадь
        center = (int(rect[0][0]),int(rect[0][1]))
        if area > 500:
            cv2.drawContours(thresh,[box],0,(180,180,180),2) # 
            cv2.circle(thresh, center, 15, (200,200,200), 2)
        

    
    
    
    cv2.imshow('result', thresh) 
 
    ch = cv2.waitKey(5)
    if ch == 27:
        break

cap.release()
cv2.destroyAllWindows()

    
